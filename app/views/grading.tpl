<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="{$conf->app_url}/css/main.css" />
</head>
<body>
<div class="inner">
    <h2 class="major">Lista ocen {$uczen[0]['Imie']} {$uczen[0]['Nazwisko']}</h2>
    <h3><button id="myBtn">Dodaj ocenę</button>
        <!-- The Modal -->
        <div id="myModal" class="modal">
        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <h2>Dodaj nową ocenę</h2>
            </div>
            <div class="modal-body">
                <p><div id="contact3" class="contact3">
                    <form class="form"  method = "post" action = "{$conf->action_root}addGrade">
                        <div class = "fields">
                            <div class = "field">
                                <label>Ocena</label>
                                <input type="text" name="Ocena" placeholder="Ocena" id="Ocena" required/>
                            </div>

                            <div class = "field">
                                <label>Komentarz</label>
                                <input type="text" name="Komentarz" placeholder="Komentarz" id="Komentarz" required/>
                            </div>
                            <div class="field">
                                <button class="formSubmit4" name="submit" type="submit">Zatwierdź</button>
                                <span class="close">&times;</span>
                            </div>
                            <input type="hidden" name="ID_Ucznia" id="ID_Ucznia" value="{$id}">
                        </div>
                        <div class="emailFormAlert"></div>
                    </form>

                </div></p>
            </div>
        </div>
        </div>
        <a class="button" href="{$conf->action_root}studentList">Wróc do listy uczniów</a>
        <button class="triggerMessage" onclick="modalClicked();">Wyślij wiadomość</button>
        <div class="Message">
            <div class="Message-content">
                <span class="Message-button" onclick="modalClicked();"  >×</span>
                <h1>Wiadomość</h1>
                <h1><textarea id="wiadomosc"></textarea></h1>
                <button id="submitMessage" class="button" onclick="sendMessage('{$conf->action_root}addMessage&id={$id}')">Wyślij</button>
            </div>
        </div>
    </h3>
        <input id="search" type="text" placeholder="Wyszukaj..." onkeyup="ajaxPostForm('{$conf->action_root}reloadGrades', 'GradesContainer');">
    <div class ="GradesContainer" id="GradesContainer">
        <input type="hidden" id="wartosc" value="{$ilosc}">
    {include "gradesList.tpl"}
    </div>
</div>
<script src="{$conf->app_url}/js/jquery.min.js"></script>
<script src="{$conf->app_url}/js/jquery.scrollex.min.js"></script>
<script src="{$conf->app_url}/js/browser.min.js"></script>
<script src="{$conf->app_url}/js/breakpoints.min.js"></script>
<script src="{$conf->app_url}/js/util.js"></script>
<script src="{$conf->app_url}/js/lista.js"></script>
<script src="{$conf->app_url}/js/przycisk.js"></script>
<script src="{$conf->app_url}/js/message.js"></script>
<script src="{$conf->app_url}/js/grade.js"></script>
<script src="{$conf->app_url}/js/updateGrade.js"></script>
<script src="{$conf->app_url}/js/deleting.js"></script>
</body>
</html>
