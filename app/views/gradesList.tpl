<table id="table">
    {*<thead>*}
    <tr>
        <th>Ocena</th>
        <th>Wystawił(a)</th>
        <th>Data modyfikacji</th>
        <th>Modyfikował(a) ostatnio</th>
        <th>Opis</th>
    </tr>
    {*</thead>*}
    {*<tbody>*}
    {foreach $lista as $l}
        {strip}
            <tr>
                <th>{$l['Ocena']}</th>
                <th>{$l['ImieWystawcy']} {$l['NazwiskoWystawcy']}</th>
                <th>{$l['Data_Modyfikacji']}</th>
                <th>{$l['ImieEdytora']} {$l['NazwiskoEdytora']}</th>
                <th>{$l['Opis']}</th>
                <td>
                    <a class="button" href="{$conf->action_root}editGrade&idOceny={$l['ID_Oceny']}">Edytuj</a>
                    <a class="button" href="{$conf->action_root}deleteGrade&idOceny={$l['ID_Oceny']}&id={$id[0]}" onclick="return deleteFunction()">Usuń</a>
                </td>
            </tr>
        {/strip}
    {/foreach}

    {*</tbody>*}
</table>