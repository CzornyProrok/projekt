<!doctype html>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="{$page_description|default:'Opis domyślny'}">
	<title>{$page_title|default:"Tytuł domyślny"}</title>
	<link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css" integrity="sha384-" crossorigin="anonymous">
	<link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/grids-responsive-min.css">
	<link rel="stylesheet" href="{$conf->app_url}/css/style.css">
	<div class="header">
        <div class="pure-menu pure-menu-horizontal">
            <a class="pure-menu-heading" href="">Librus</a>

            <ul class="pure-menu-list">
                <li class="pure-menu-item pure-menu-selected"><a href="#" class="pure-menu-link">{$page_title|default:"Tytuł domyślny"}</a></li>
                <li class="pure-menu-item"><a href="https://www.smarty.net/" class="pure-menu-link">{$page_header|default:"Tytuł domyślny"}</a></li>
                <li class="pure-menu-item"><a href="#" class="pure-menu-link">{$page_description|default:"Opis domyślny"}</a></li>
            </ul>
        </div>
    </div>

</head>
<body>

{block name=content} Domyślna treść zawartości .... {/block}


<div class="footer">
	<p>
{block name=footer}{/block}
	</p>
</div>

</body>
</html>