<!DOCTYPE HTML>
<html lang="pl">

<head>
    <title>Rejestracja</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="{$conf->app_url}/css/main.css" />
   <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"> -->
    <noscript><link rel="stylesheet" href="{$conf->app_url}/css/noscript.css" /></noscript>
</head>

<body class="is-preload">
<!-- Page Wrapper -->
<div id="page-wrapper">

    <!-- Header -->
    <header id="header" class="alt">
        <h1><a href="#">Uczniowie</a></h1>
        <nav>
            <button id="open-contact-btn2" class="about-link">
                <i class="fa fa-envelope-o" aria-hidden="true"></i> Formularz rejestracji
            </button>
            <div id="contact2" class="contact2">
                        <form id ="form2" class="form"  method = "post" action = "{$conf->action_root}studentSave">
                            <div class = "fields">
                                <div class = "field">
                                    <input type="text" name="Imie" placeholder="Imie" id="Imie" required/>
                                </div>
                                <div class = "field">
                                    <input type="text" name="Nazwisko" placeholder="Nazwisko" id="Nazwisko" required/>
                                </div>
                                <div class = "field">
                                    <input type="text" name="Pesel" placeholder="PESEL" id="Pesel" required/>
                                </div>
                                <div class = "field">
                                    <input type="text" name="Klasa" placeholder="Klasa (np. 2)" id="Klasa" required/>
                                </div>
                                <div class = "field">
                                    <input type="text" name="Oddzial" placeholder="Oddział (np. 2A)" id="Oddzial" required/>
                                </div>
                                <div class="field">
                                <button class="formSubmit" name="submit" type="submit">Utwórz</button>
                                    <button type="button" id="close-contact-btn2" class="close-btn">
                                        <i class="fa fa-times close-form" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="emailFormAlert"></div>
                        </form>

            </div>
            <div class="button"><a href="{$conf->action_root}librus">Strona główna</a><div>
        </nav>
    </header>

    <!-- Banner -->
    <section id="banner">
        <div class="inner">
            <div class="logo"><span class="icon fa-diamond"></span></div>
            <h2>Witamy w panelu</h2>
            <p>Wyedytuj dane, dodaj lub usuń ucznia.</p>
        </div>
    </section>
    <section id="four" class="wrapper alt style1">
        <div class="inner">
            <h2 class="major">Lista uczniów</h2>
                <input id="search" type="text" placeholder="Wyszukaj..." onkeyup="ajaxPostForm('{$conf->action_root}reloadStudent', 'studentContainer');">
            <div class="studentContainer" id="studentContainer">
                {include "studentList.tpl"}
            </div>

        </div>
            </section>
        </div>
    </section>
    <!-- Footer -->
    <section id="footer">
        <div class="inner">
            <h2 class="major">Błąd na stronie?</h2>
            <p>Zgłoś administratorowi!</p>
            <ul class="contact">
                <li class="fa-home">
                    Kompania Braci<br />
                    ul. Drogowa 40<br />
                    Jaworzno, 43-600
                </li>
                <li class="fa-phone">511-420-778</li>
                <li class="fa-envelope"><a href="#">maciejkna470@gmail.com</a></li>
                <li class="fa-twitter"><a href="#">no twitter</a></li>
                <li class="fa-facebook"><a href="#">no facebook</a></li>
                <li class="fa-instagram"><a href="#">no instagram</a></li>
            </ul>
            <ul class="copyright">
                <li>&copy; Untitled Inc. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
            </ul>
        </div>
    </section>

</div>

<!-- Scripts -->
<script src="{$conf->app_url}/js/jquery.min.js"></script>
<script src="{$conf->app_url}/js/jquery.scrollex.min.js"></script>
<script src="{$conf->app_url}/js/browser.min.js"></script>
<script src="{$conf->app_url}/js/breakpoints.min.js"></script>
<script src="{$conf->app_url}/js/util.js"></script>
<script src="{$conf->app_url}/js/main.js"></script>
<script src="{$conf->app_url}/js/form.js"></script>
<script src="{$conf->app_url}/js/lista2.js"></script>
<script src="{$conf->app_url}/js/paginator.js"></script>
<script src="{$conf->app_url}/js/deleting.js"></script>
</body>
</html>