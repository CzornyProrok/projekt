<!DOCTYPE HTML>
<html lang="pl">
<head>
    <title>Librus</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="{$conf->app_url}/css/main.css" />
    <noscript><link rel="stylesheet" href="{$conf->app_url}/css/noscript.css" /></noscript>
</head>
<body class = "is-preload">
<a class="button" href="{$conf->action_root}librus" style="float: right">Strona główna</a>
<h2>LISTA OCEN</h2>
<table id="table">
    {*<thead>*}
    <tr>
        <th>Ocena</th>
        <th>Wystawił(a)</th>
        <th>Data modyfikacji</th>
        <th>Modyfikował(a) ostatnio</th>
        <th>Przedmiot</th>
        <th>Opis</th>
    </tr>
    {*</thead>*}
    {*<tbody>*}
    {foreach $lista as $l}
        {strip}
            <tr>
                <th>{$l['Ocena']}</th>
                <th>{$l['ImieWystawcy']} {$l['NazwiskoWystawcy']}</th>
                <th>{$l['Data_Modyfikacji']}</th>
                <th>{$l['ImieEdytora']} {$l['NazwiskoEdytora']}</th>
                <th>{$l['Przedmiot']}</th>
                <th>{$l['Opis']}</th>
            </tr>
        {/strip}
    {/foreach}

    {*</tbody>*}
</table>
</body>
</html>