<!DOCTYPE HTML>
<html lang = 'pl'>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="{$conf->app_url}/css/main.css" />
</head>
<body>
<div id="contact3" class="contact3">
    <form class="form"  method = "post" action = "{$conf->action_root}studentUpdate">
        <div class = "fields">
            <div class = "field">
                <label>Imię</label>
                <input type="text" name="Imie" placeholder="Imie" id="Imie" value="{$form->Imie}" required/>
            </div>
            <div class = "field">
                <label>Nazwisko</label>
                <input type="text" name="Nazwisko" placeholder="Nazwisko" id="Nazwisko" value="{$form->Nazwisko}" required/>
            </div>
            <div class = "field">
                <label>PESEL</label>
                <input type="text" name="Pesel" placeholder="PESEL" id="Pesel" value="{$form->Pesel}" required/>
            </div>
            <div class = "field">
                <label>Klasa</label>
                <input type="text" name="Klasa" placeholder="Klasa (np. 2)" id="Klasa" value="{$form->Klasa}" required/>
            </div>
            <div class = "field">
                <label>Oddział</label>
                <input type="text" name="Oddzial" placeholder="Oddział (np. 2A)" id="Oddzial" value="{$form->Oddzial}" required/>
            </div>
            <div class = "field">
                <input type="hidden" name="id" id="id" value="{$form->ID_Ucznia}">
            </div>
            <div class="field">
                <button class="formSubmit2" name="submit" type="submit">Zatwierdź</button>
                <a class="button" href="{$conf->action_root}studentList">Anuluj</a>
            </div>

        </div>
        <div class="emailFormAlert"></div>
    </form>

</div>
<script src="{$conf->app_url}/js/jquery.min.js"></script>
<script src="{$conf->app_url}/js/jquery.scrollex.min.js"></script>
<script src="{$conf->app_url}/js/browser.min.js"></script>
<script src="{$conf->app_url}/js/breakpoints.min.js"></script>
<script src="{$conf->app_url}/js/util.js"></script>
<script src="{$conf->app_url}/js/update.js"></script>
</body>
</html>