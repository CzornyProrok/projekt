<!DOCTYPE HTML>

<html lang="pl">
<head>
	<title>Librus</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="{$conf->app_url}/css/main.css" />
	<noscript><link rel="stylesheet" href="{$conf->app_url}/css/noscript.css" /></noscript>
</head>
<body class="is-preload">
<!-- Page Wrapper -->
<div id="page-wrapper">

	<!-- Header -->
	<header id="header" class="alt">
		<h1><a href="index.html">Librus</a></h1>
		<nav>
			<a href="#menu">Zaloguj</a>
		</nav>
	</header>
	<!-- Menu -->
	<nav id="menu">
		<div class="inner">
			<h2>Logowanie</h2>
			<div class="links">
				<form method="post" action="{$conf->action_root}login">
						<fieldset>
						<div class="fields">
							<div class="field">
								<label for="login">PESEL</label>
								<input type="text" name="login" id="login" />
							</div>

							<div class="field">
								<label for="pass">Hasło</label>
								<input type="password" name="pass" id="pass" />
							</div>

							<button input type ="submit" id="myBtn">Zaloguj</button>

								<!-- The Modal -->
								<div id="myModal" class="modal">

									<!-- Modal content -->
									<div class="modal-content">
										<div class="modal-header">
											<span class="close">&times;</span>
											<h2>Komunikat</h2>
										</div>
										<div class="modal-body">
											<p>Logowanie...</p>
										</div>
										<div class="modal-footer">
											<h3></h3>
										</div>
									</div>

								</div>

							</div>
						</div>
							{if $msgs->isError()}
								<div class="messages error bottom-margin">
									<ul>
										{foreach $msgs->getErrors() as $msg}
											{strip}
												<li>{$msg}</li>
											{/strip}
										{/foreach}
									</ul>
								</div>
							{/if}
						</fieldset>
					</form>
			<a href="#" class="close">Close</a>
		</div>
	</nav>

	<!-- Banner -->
	<section id="banner">
		<div class="inner">
			<div class="logo"><span class="icon fa-diamond"></span></div>
			<h2>Witamy w Librusie</h2>
			<p>Najlepsze narzędzie do uprzykrzania życia Twojemu dziecku! </p>
		</div>
	</section>

	<!-- Wrapper -->
	<section id="wrapper">

		<!-- One -->
		<section id="one" class="wrapper spotlight style1">
			<div class="inner">
				<a href="#" class="image"><img src="images/pic01.jpg" alt="" /></a>
				<div class="content">
					<h2 class="major">Poznaj oceny swojego dziecka przed nim</h2>
					<p>Masz dość sytuacji gdy dziecko wraca do domu i chwali się piątką, lecz nie wspomina o jedynce? Teraz się to zmieni! Jedynki zawsze mają priorytet wpisu, jak to w Librusie!</p>
					<!--<a href="#" class="special">Learn more</a>-->
				</div>
			</div>
		</section>

		<!-- Two -->
		<section id="two" class="wrapper alt spotlight style2">
			<div class="inner">
				<a href="#" class="image"><img src="images/pic02.jpg" alt="" /></a>
				<div class="content">
					<h2 class="major">Wydaje Ci się, że dziecko oszukuje, na którą ma do szkoły?</h2>
					<p>To już też nie problem! Dzięki Librusowi masz wgląd w plan zajęć swojego dziecka!</p>
					<!--<a href="#" class="special">Learn more</a>-->
				</div>
			</div>
		</section>

		<!-- Three -->
		<!--<section id="three" class="wrapper spotlight style3">
			<div class="inner">
				<a href="#" class="image"><img src="images/pic03.jpg" alt="" /></a>
				<div class="content">
					<h2 class="major">Nullam dignissim</h2>
					<p>Lorem ipsum dolor sit amet, etiam lorem adipiscing elit. Cras turpis ante, nullam sit amet turpis non, sollicitudin posuere urna. Mauris id tellus arcu. Nunc vehicula id nulla dignissim dapibus. Nullam ultrices, neque et faucibus viverra, ex nulla cursus.</p>
					<a href="#" class="special">Learn more</a>
				</div>
			</div>
		</section>
-->
		<!-- Four -->
		<section id="four" class="wrapper alt style1">
			<div class="inner">
				<h2 class="major">Dodatkowe informacje</h2>
				<p>Sprawdź też:</p>
				<section class="features">
					<article>
						<a class="image"><img src="{$conf->app_url}/images/plan.jpg" alt="" /></a>
						<h3 class="major">Plan zajęć</h3>
						<p></p>
						<a href="#" class="special">Przejdź do planu</a>
					</article>
					<article>
						<a class="image"><img src="{$conf->app_url}/images/kadra.jpg" alt="" /></a>
						<h3 class="major">Kadra nauczycielska</h3>
						<p></p>
						<a href="#" class="special">Przejrzyj kadrę</a>
					</article>

				</section>
				<ul class="actions">
					<li><a href="#" class="button">Browse All</a></li>
				</ul>
			</div>
		</section>

	</section>

	<!-- Footer -->
	<section id="footer">
		<div class="inner">
			<h2 class="major">Błąd na stronie?</h2>
			<p>Zgłoś administratorowi!</p>
			<!--<form method="post" action="#">
				<div class="fields">
					<div class="field">
						<label for="name">Name</label>
						<input type="text" name="name" id="name" />
					</div>
					<div class="field">
						<label for="email">Email</label>
						<input type="email" name="email" id="email" />
					</div>
					<div class="field">
						<label for="message">Message</label>
						<textarea name="message" id="message" rows="4"></textarea>
					</div>
				</div>
				<ul class="actions">
					<li><input type="submit" value="Send Message" /></li>
				</ul>
			</form>-->
			<ul class="contact">
				<li class="fa-home">
					Kompania Braci<br />
					ul. Drogowa 40<br />
					Jaworzno, 43-600
				</li>
				<li class="fa-phone">511-420-778</li>
				<li class="fa-envelope"><a href="#">maciejkna470@gmail.com</a></li>
				<li class="fa-twitter"><a href="#">no twitter</a></li>
				<li class="fa-facebook"><a href="#">no facebook</a></li>
				<li class="fa-instagram"><a href="#">no instagram</a></li>
			</ul>
			<ul class="copyright">
				<li>&copy; Untitled Inc. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
			</ul>
		</div>
	</section>

</div>

<!-- Scripts -->
<script src="{$conf->app_url}/js/jquery.min.js"></script>
<script src="{$conf->app_url}/js/jquery.scrollex.min.js"></script>
<script src="{$conf->app_url}/js/browser.min.js"></script>
<script src="{$conf->app_url}/js/breakpoints.min.js"></script>
<script src="{$conf->app_url}/js/util.js"></script>
<script src="{$conf->app_url}/js/main.js"></script>
</body>
</html>