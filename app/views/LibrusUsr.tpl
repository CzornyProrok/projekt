<!DOCTYPE HTML>
<html lang="pl">
<head>
    <title>Librus</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="{$conf->app_url}/css/main.css" />
    <noscript><link rel="stylesheet" href="{$conf->app_url}/css/noscript.css" /></noscript>
</head>
<body class = "is-preload">
<!-- Page Wrapper -->
<div id="page-wrapper">

    <!-- Header -->
    <header id="header" class="alt">
        <h1><a href="index.html">Librus</a></h1>
        <nav>
            <a href="#menu">Menu</a>
            <a href="#menu2">Wyloguj</a>
        </nav>
    </header>

    <!-- Menu -->
    <nav id="menu">
        <div class="inner">
            <h2>Menu ocen</h2>
            <ul class="links">
                <li><a href="{$conf->action_root}showGradesUser&id={$sesID}">Lista ocen</a></li>
                <li><a href="{$conf->action_root}showMessageUser&id={$sesID}">Wiadomości</a></li>
                <li><a href="elements.html">soon</a></li>
            </ul>
        </div>
    </nav>
    <nav id="menu2">
        <div class="inner">
            <h2>Wyloguj</h2>
            <div class="links">
                <p>Czy na pewno chcesz się wylogować?</p>
                <li><a href="{$conf->action_root}logout">Wyloguj</a></li>
            </div>
        </div>
    </nav>
</div>
<!-- Wrapper -->
<section id="wrapper">


    <!-- Four -->
    <section id="four" class="wrapper alt style1">
        <div class="inner">
            <h2 class="major">Dodatkowe informacje</h2>
            <p>Sprawdź też:</p>
            <section class="features">
                <article>
                    <a class="image"><img src="{$conf->app_url}/images/plan.jpg" alt="" /></a>
                    <h3 class="major">Plan zajęć</h3>
                    <p></p>
                    <a href="#" class="special">Wkrótce</a>
                </article>
                <article>
                    <a class="image"><img src="{$conf->app_url}/images/kadra.jpg" alt="" /></a>
                    <h3 class="major">Kadra nauczycielska</h3>
                    <p></p>
                    <a href="#" class="special">wkrótce</a>
                </article>

            </section>
        </div>
    </section>

</section>
<script src="{$conf->app_url}/js/jquery.min.js"></script>
<script src="{$conf->app_url}/js/jquery.scrollex.min.js"></script>
<script src="{$conf->app_url}/js/browser.min.js"></script>
<script src="{$conf->app_url}/js/breakpoints.min.js"></script>
<script src="{$conf->app_url}/js/util.js"></script>
<script src="{$conf->app_url}/js/main.js"></script>
</body>
</html>