<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="{$conf->app_url}/css/main.css" />
</head>
    <body>
    <a class="button" href="{$conf->action_root}librus">Strona główna</a>
        </div>
<table id="tab_temporary" style='border-collapse: collapse'>

    {foreach $hasla as $h}
        {strip}
    <thead style='border: 1px solid #dddddd'>
    <tr style='border: 1px solid #dddddd'>
        <th style='text-align:center;vertical-align:middle;'>Imię</th>
        <th style='text-align:center;vertical-align:middle'>Nazwisko</th>
        <th style='text-align:center;vertical-align:middle'>PESEL</th>
        <th style='text-align:center;vertical-align:middle'>Hasło tymczasowe</th>
    </tr>
    </thead>
    <tbody id="myTable" style ='border: 1px solid #dddddd'>
            <tr style='border: 1px solid #dddddd'>
                <td>{$h['Imie']}</td>
                <td>{$h['Nazwisko']}</td>
                <td>{$h['Pesel']}</td>
                <td>{$h['Haslo']}</td>
            </tr>
    </tbody>
        {/strip}
    {/foreach}
</table>
    </body>
</html>