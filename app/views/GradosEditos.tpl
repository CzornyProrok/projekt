<!DOCTYPE HTML>
<html lang = 'pl'>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="{$conf->app_url}/css/main.css" />
</head>
<body>
<div id="contact3" class="contact3">
    <form class="form"  method = "post" action = "{$conf->action_root}updateGrade">
        <div class = "fields">
            <div class = "field">
                <label>Ocena</label>
                <input type="text" name="Ocena" placeholder="Ocena" id="Ocena" value="{$form->Ocena}" required/>
            </div>
            <div class = "field">
                <label>Komentarz</label>
                <input type="text" name="Komentarz" placeholder="Komentarz" id="Komentarz" value="{$form->Komentarz}" required/>
            </div>
            <div class = "field">
                <input type="hidden" name="id" id="id" value="{$form->ID_Ucznia}">
            </div>
            <div class = "field">
                <input type="hidden" name="idOceny" id="idOceny" value="{$form->ID_Oceny}">
            </div>
            <div class="field">
                <button class="formSubmit5" name="submit" type="submit">Zatwierdź</button>
                <a class="button" href="{$conf->action_root}showGrades&id={$form->ID_Ucznia}">Wróc do listy ocen</a>
            </div>
        </div>
        <div class="emailFormAlert"></div>
    </form>

</div>
<script src="{$conf->app_url}/js/jquery.min.js"></script>
<script src="{$conf->app_url}/js/jquery.scrollex.min.js"></script>
<script src="{$conf->app_url}/js/browser.min.js"></script>
<script src="{$conf->app_url}/js/breakpoints.min.js"></script>
<script src="{$conf->app_url}/js/util.js"></script>
<script src="{$conf->app_url}/js/updateGrade.js"></script>
</body>
</html>