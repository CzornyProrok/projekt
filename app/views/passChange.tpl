<!DOCTYPE HTML>
<html lang = 'pl'>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="{$conf->app_url}/css/main.css" />
</head>
<body>
<div id="contact4" class="contact4">
    <form class="form"  method = "post" action = "{$conf->action_root}passChangeDo">
        <div class = "fields">
            <div class = "field">
                <label>Nowe hasło:</label>
                <input type="password" name="newPass" placeholder="Nowe hasło(musi mieć przynajmniej 8 znaków, nie może zawierać znaków specjalnych, w tym spacji)" id="newPass"  required/>
            </div>

            <div class = "field">
                <label>Powtórz nowe hasło:</label>
                <input type="password" name="newPassR" placeholder="Powtórz nowe hasło" id="newPassR" required/>
            </div>
                <button class="formSubmit3" name="submit" type="submit">Zatwierdź</button>
            </div>
        </div>
        <div class="emailFormAlert"></div>
    </form>

</div>
<script src="{$conf->app_url}/js/jquery.min.js"></script>
<script src="{$conf->app_url}/js/jquery.scrollex.min.js"></script>
<script src="{$conf->app_url}/js/browser.min.js"></script>
<script src="{$conf->app_url}/js/breakpoints.min.js"></script>
<script src="{$conf->app_url}/js/util.js"></script>
<script src="{$conf->app_url}/js/pass.js"></script>
</body>
</html>