<table>
    Znaleziono {$iloscWynikow} wyników
    <thead>
    <tr>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>PESEL</th>
        <th>Klasa</th>
        <th>Oddział</th>
    </tr>
    </thead>
    <tbody id="myTable">
    {foreach $lista as $l}
        {strip}
            <tr>
                <th>{$l['Imie']}</th>
                <th>{$l['Nazwisko']}</th>
                <th>{$l['Pesel']}</th>
                <th>{$l['Klasa']}</th>
                <th>{$l['Oddzial']}</th>
                <td>
                    <a class="button" href="{$conf->action_root}studentEdit&id={$l['ID_Ucznia']}">Edytuj</a>
                    <a class="button" href="{$conf->action_root}studentDelete&id={$l['ID_Ucznia']}" onclick="return deleteFunction()">Usuń</a>
                    <a class="button" href="{$conf->action_root}showGrades&id={$l['ID_Ucznia']}">Oceny/Wiadomości</a>
                </td>
            </tr>
        {/strip}
    {/foreach}
    </tbody>
</table>
<ul class="pagination">
    {for $foo=1 to {$iloscStron}}
        <li><input type="button" id="strona" onclick="ajaxPagination('{$conf->action_root}reloadStudent&currentSite={$foo}', 'studentContainer')" value="{$foo}"></li>
    {/for}
</ul>