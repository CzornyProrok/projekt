<!DOCTYPE HTML>
<html lang="pl">
<head>
    <title>Librus</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="{$conf->app_url}/css/main.css" />
    <noscript><link rel="stylesheet" href="{$conf->app_url}/css/noscript.css" /></noscript>
</head>
<body>
<a class="button" href="{$conf->action_root}librus" style="float: right">Strona główna</a>
<h2>WIADOMOŚCI</h2>
<table id="table">
    {*<thead>*}
    <tr>
        <th>OD</th>
        <th>DO</th>
        <th>Data</th>
        <th>Treść</th>
    </tr>
    {*</thead>*}
    {*<tbody>*}
    {foreach $wiadomosc as $l}
        {strip}
            <tr>
                <th>{$l['NazwiskoPracownika']} {$l['NazwiskoPracownika']}</th>
                <th>{$l['ImieUcznia']} {$l['NazwiskoUcznia']}</th>
                <th>{$l['Data_Wiadomosci']}</th>
                <th>{$l['Wiadomosc']}</th>
            </tr>
        {/strip}
    {/foreach}

    {*</tbody>*}
</table>
</body>
</html>