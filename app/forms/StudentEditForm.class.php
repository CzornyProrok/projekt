<?php

namespace app\forms;

class StudentEditForm{
    public $ID_Ucznia;
    public $Imie;
    public $Nazwisko;
    public $Pesel;
    public $Haslo;
    public $ID_Modyfikatora;
    public $Data_Modyfikacji;
    public $Klasa;
    public $Oddzial;
}