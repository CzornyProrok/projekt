<?php

namespace app\forms;

class GradeForm {
    public $ID_Oceny;
    public $ID_Ucznia;
    public $Ocena;
    public $Data_Modyfikacji;
    public $Komentarz;
    public $ID_Modyfikatora;
}