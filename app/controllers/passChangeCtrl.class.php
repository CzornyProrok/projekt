<?php

namespace app\controllers;

use app\forms\PassChangeForm;
use PDOException;

class passChangeCtrl
{
    private $form;

    public function __construct()
    {
        $this->form = new PassChangeForm();
    }

    public function validation()
    {
        $this->form->newPass = getFromRequest('newPass');
        $this->form->newPassR = getFromRequest('newPassR');

        if($this->form->newPass != $this->form->newPassR)
        {
            $response = "podane hasła różnią się";
            exit($response);
            return false;
        }
        else if($this->form->newPass == $this->form->newPassR)
        {
            if(strlen($this->form->newPass)<8)
            {
                $response = "Podane hasło jest za krótkie";
                exit($response);
                return false;
            }
            else if(strlen($this->form->newPass)>=8)
            {
                if(!preg_match('/^[a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ0-9]+$/', $this->form->newPass))
                {
                    $response = "Podane hasło zawiera niepoprawne znaki";
                    exit($response);
                    return false;
                }
                else return true;
            }
        }

    }
public function action_passChange()
{
    $this->generateView();
}
    public function action_passChangeDo()
    {
        if($this->validation())
        {
            try
            {
                if(getDB()->has("uczen", ["AND" => [ "ID_Ucznia" => $_SESSION['ID'][0], "Haslo" => $this->form->newPass]]))
                {
                    echo "hasło nie różni się od poprzedniego";
                }
                else if(!getDB()->has("uczen", ["AND" => [ "ID_Ucznia" => $_SESSION['ID'][0], "Haslo" => $this->form->newPass]]))
                {
                    getDB()->update("uczen", [
                        "Haslo" => $this->form->newPass,
                        "Tymczasowe" => 0
                    ], [
                        "ID_Ucznia" => $_SESSION['ID'][0]
                    ]);
                    //forwardTo('librus');
                }
            }
            catch(PDOException $e)
            {

            }
        }
        //$this->generateView();

    }
    public function generateView()
    {
        getSmarty()->assign('form',$this->form); // dane formularza dla widoku
        getSmarty()->display('passChange.tpl');
    }
}