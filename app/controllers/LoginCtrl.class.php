<?php

namespace app\controllers;

use app\forms\LoginForm;

class LoginCtrl
{

	private $form;
	public function __construct()
    {
		//stworzenie potrzebnych obiektów
		$this->form = new LoginForm();
    }

	public function validate()
    {
        $this->form->login = getFromRequest('login');
        $this->form->pass = getFromRequest('pass');
        if (!isset($this->form->login))
        {

            return false;
        }

        // sprawdzenie, czy potrzebne wartości zostały przekazane
        if (empty($this->form->login))
        {
            getMessages()->addError('Nie podano loginsssu');
            return false;
        }
        if (empty($this->form->pass))
        {
            getMessages()->addError('Nie podano hasła');
            return false;
        }

        if (getMessages()->isError())
        {
            return false;
        }
        // sprawdzenie, czy dane logowania poprawne
        // (takie informacje najczęściej przechowuje się w bazie danych)
        $logowanie = getDB()->count("pracownik", ["AND" => ["Pesel" => $this->form->login, "Haslo" => $this->form->pass]]);
        $logowanie2 = getDB()->count("uczen", ["AND" => ["Pesel" => $this->form->login, "Haslo" => $this->form->pass]]);
        if ($logowanie==1)
        {

            $_SESSION['ID'][0] = getDB()->get("pracownik", "ID_Pracownika", ["Pesel" => $this->form->login]);
            addRole('admin');
            return true;
        } else if ($logowanie2 == 1)
        {
            addRole('user');
            $_SESSION['ID'][0] = getDB()->get("uczen", "ID_Ucznia", ["Pesel" => $this->form->login]);
            if(getDB()->has("uczen", ["AND" => ["Pesel" => $this->form->login, "Tymczasowe" => 1]]))
            {
                redirectTo('passChange');
            }
            return true;
        }
        else
        {
            getMessages()->addError('Niepoprawny login lub hasło');
            return false;

        }
        return ! getMessages()->isError();
    }



	public function action_login()
    {

		if ($this->validate())
		{
            sleep(1);
            redirectTo("librus");
		} else
		    {
                sleep(1);
                $this->generateView();

		    }

	}

	public function action_logout(){
		// 1. zakończenie sesji
		session_destroy();
		// 2. idź na stronę główną - system automatycznie przekieruje do strony logowania
		redirectTo('librus');
	}
	
	public function generateView(){
		getSmarty()->assign('form',$this->form); // dane formularza do widoku
		getSmarty()->display('LoginView.tpl');
	}
}