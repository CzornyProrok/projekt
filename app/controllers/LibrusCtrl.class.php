<?php

namespace app\controllers;


class LibrusCtrl
{

    public function action_librus()
    {
        if(!inRole("admin"))        //sprawdzenie czy user ma rolę - jak nie, to trzeba się zalogować
        {
            if(!inRole("user"))
            {
                redirectTo('login');
            }

        }

        if(inRole("admin"))
        {
            getSmarty()->assign('sesID', $_SESSION['ID'][0]);
            getSmarty()->display('LibrusAdm.tpl');
        }

        if(inRole("user"))
        {
            getSmarty()->assign('sesID', $_SESSION['ID'][0]);
            getSmarty()->display('LibrusUsr.tpl');
        }
    }
}