<?php

namespace app\controllers;

use app\forms\GradeForm;
use PDOException;

class GradeCtrl
{
    private $form;
    private $records;
    private $uczen;
    private $zmienna;
    public function __construct()
    {
        $this->form = new GradeForm();
    }
public function action_showGrades()
{
    $this->form->ID_Ucznia = getFromRequest('id');
    try
    {
        $this->uczen = getDB()->select("uczen", [
            "Imie",
            "Nazwisko"
        ],
            [
               "ID_Ucznia" => $this->form->ID_Ucznia
            ]);
        $this->records = getDB()->select("ocena", [
            "[><]pracownik(p)" => ["ID_Pracownika" => "ID_Pracownika"],
            "[><]pracownik(pr)" => ["ID_Modyfikatora" => "ID_Pracownika"],
            "[><]uczen(u)" => ["ID_Ucznia" => "ID_Ucznia"]
        ],["ocena.Ocena",
                "ocena.ID_Oceny",
                "p.Imie(ImieWystawcy)",
                "p.Nazwisko(NazwiskoWystawcy)",
                "ocena.Data_Modyfikacji",
                "pr.Imie(ImieEdytora)",
                "pr.Nazwisko(NazwiskoEdytora)",
                "ocena.Opis",
                "u.Imie(ImieUcznia)",
                "u.Nazwisko(NazwiskoUcznia)"
            ],[
                    "ocena.ID_Ucznia" => $this->form->ID_Ucznia,
                ]);
    }
    catch(PDOException $e)
    {

    }
    getSmarty()->assign('id', $this->form->ID_Ucznia);
    getSmarty()->assign('uczen', $this->uczen);
    getSmarty()->assign('lista', $this->records);
    getSmarty()->display('grading.tpl');
    //forwardTo('reloadGrades');
}
    public function action_showGradesUser()
    {
        $this->form->ID_Ucznia = getFromRequest('id');
        try
        {
            $this->uczen = getDB()->select("uczen", [
                "Imie",
                "Nazwisko"
            ],
                [
                    "ID_Ucznia" => $this->form->ID_Ucznia
                ]);
            $this->records = getDB()->select("ocena", [
                "[><]pracownik(p)" => ["ID_Pracownika" => "ID_Pracownika"],
                "[><]pracownik(pr)" => ["ID_Modyfikatora" => "ID_Pracownika"],
                "[><]uczen(u)" => ["ID_Ucznia" => "ID_Ucznia"]
            ],["ocena.Ocena",
                "ocena.ID_Oceny",
                "p.Imie(ImieWystawcy)",
                "p.Nazwisko(NazwiskoWystawcy)",
                "ocena.Data_Modyfikacji",
                "pr.Imie(ImieEdytora)",
                "pr.Nazwisko(NazwiskoEdytora)",
                "ocena.Opis",
                "u.Imie(ImieUcznia)",
                "u.Nazwisko(NazwiskoUcznia)",
                "p.Przedmiot"
            ],["ORDER" => ["ocena.Data_Modyfikacji" => "DESC"],
                "ocena.ID_Ucznia" => $this->form->ID_Ucznia]
             );
        }
        catch(PDOException $e)
        {

        }
        getSmarty()->assign('uczen', $this->uczen);
        getSmarty()->assign('lista', $this->records);
        getSmarty()->display('gradesUser.tpl');
        //forwardTo('reloadGrades');
    }
    public function action_reloadGrades()
    {

        $this->zmienna = explode(" ", getFromRequest('search'));
        $this->form->ID_Ucznia = getFromRequest('id');
        try
        {
            $this->records = getDB()->select("ocena", [
                "[><]pracownik(p)" => ["ID_Pracownika" => "ID_Pracownika"],
                "[><]pracownik(pr)" => ["ID_Modyfikatora" => "ID_Pracownika"],
                "[><]uczen(u)" => ["ID_Ucznia" => "ID_Ucznia"]],
                ["ocena.Ocena",
                "ocena.ID_Oceny",
                "p.Imie(ImieWystawcy)",
                "p.Nazwisko(NazwiskoWystawcy)",
                "ocena.Data_Modyfikacji",
                "pr.Imie(ImieEdytora)",
                "pr.Nazwisko(NazwiskoEdytora)",
                    "ocena.Opis",
                    "u.Imie(ImieUcznia)",
                    "u.Nazwisko(NazwiskoUcznia)"
            ],[
                "AND" =>[

                    "ocena.ID_Ucznia" => $this->form->ID_Ucznia,

                       "OR" => [
                           "p.Imie[~]" => $this->zmienna,
                           "ocena.Opis[~]" => $this->zmienna,
                           "p.Nazwisko[~]" => $this->zmienna,
                           "pr.Imie[~]" => $this->zmienna,
                           "pr.Nazwisko[~]" => $this->zmienna,
                           "ocena.Ocena[~]" => $this->zmienna,
                           "ocena.Data_Modyfikacji[~]" => $this->zmienna,
                       ]





         ]]);
        }
        catch(PDOException $e) {

        }
        getSmarty()->assign('lista', $this->records);
        getSmarty()->display('gradesList.tpl');

    }

    public function editValid()
    {
        $this->form->ID_Oceny = getFromRequest('idOceny');
        return !getMessages()->isError();
    }

    public function action_editGrade()
    {
        if($this->editValid())
        {
            try
            {// 2. odczyt z bazy danych osoby o podanym ID (tylko jednego rekordu)
                $record = getDB()->get("ocena", "*",
                    [
                        "ID_Oceny" => $this->form->ID_Oceny
                    ]);
                // 2.1 jeśli osoba istnieje to wpisz dane do obiektu formularza
                $this->form->Ocena =$record['Ocena'];
                $this->form->Komentarz=$record['Opis'];
                $this->form->ID_Ucznia=$record['ID_Ucznia'];
                $this->form->ID_Oceny=$record['ID_Oceny'];
                getSmarty()->assign('form', $this->form);
            }
            catch(PDOException $e)
            {
                echo "<script type='text/javascript'>alert('Błąd gruby');</script>";
            }

        }
        // 3. Wygenerowanie widoku
        getSmarty()->display('GradosEditos.tpl');
    }

    public function action_updateGrade()
    {

        if ($this->validateGrade())
        {
            if($this->editValid())
            {

                try
                {
                    //2.1 Nowy rekord
                    getDB()->update("ocena", [
                        "Ocena" => $this->form->Ocena,
                        "ID_Modyfikatora" => $_SESSION['ID'][0],
                        "Data_Modyfikacji" => $this->form->Data_Modyfikacji = date('Y.m.d'),
                        "Opis" => $this->form->Komentarz
                    ], [
                        "ID_Oceny" =>$this->form->ID_Oceny
                    ]);
                   // forwardTo('showGrades');
                }
                catch(PDOException $e)
                {
                    echo "błąd";
                    if (getConf()->debug) getMessages()->addError($e->getMessage());
                }
                forwardTo('showGrades');
            }
        }

        else {
            // 3c. Gdy błąd walidacji to pozostań na stronie
            //echo "<script>alert('$this->form');</script>";
            //$alert = "błąd";
            //$response = $alert;

            //exit($response);
            forwardTo('studentEdit');
        }
    }

    public function validateGrade()
    {
        $this->form->ID_Ucznia = getFromRequest('ID_Ucznia');
        $this->form->Ocena = getFromRequest('Ocena');
        $this->form->Data_Modyfikacji = date('Y.m.d');
        $this->form->ID_Modyfikatora = $_SESSION['ID'][0];
        $this->form->Komentarz = getFromRequest('Komentarz');
        if(!preg_match("/^[0-6]{1}$/", $this->form->Ocena))
        {
            if(!preg_match("/^[0-5]{1}[-+]{1}$/", $this->form->Ocena))
            {
                if(!preg_match("/^[0-5]{1}[-]{1}[-]{1}$/", $this->form->Ocena))
                {

                    return false;
                }
            }
        }
        return true;

    }

    public function action_addGrade()
    {
        if($this->validateGrade())
        {var_dump ($this->form);
           try
           {
               getDB()->insert("ocena", [
                   "Ocena" => $this->form->Ocena,
                   "ID_Ucznia" => $this->form->ID_Ucznia,
                   "ID_Modyfikatora" => $this->form->ID_Modyfikatora,
                   "Data_Modyfikacji" => $this->form->Data_Modyfikacji,
                   "ID_Pracownika" => $this->form->ID_Modyfikatora,
                   "Opis" => $this->form->Komentarz
               ]);

               echo "dodano";
               //exit(forwardTo('grading.tpl'));
           }
           catch (PDOException $e)
           {
               echo "błąd podczas dodawania rekordu";
              // exit(forwardTo('grading.tpl'));
           }
        }
    }
    public function action_deleteGrade()
    {
        if($this->editValid())
        {
            try
            {
                getDB()->delete("ocena",
                    [
                       "ID_Oceny" => $this->form->ID_Oceny
                    ]);
                forwardTo('showGrades');
            }
            catch(PDOException $e)
            {
                echo "<script type='text/javascript'>alert('nie da się i ni mo');</script>";
            }
        }forwardTo('showGrades');

    }

//    public function generateView()
//    {
//        getSmarty()->assign('form', $this->form);
//        getSmarty()->display('grading.tpl');
//    }
}