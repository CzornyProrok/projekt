<?php

namespace app\controllers;

use app\forms\StudentEditForm;
use DateTime;
use PDOException;
use app\controllers\LoginCtrl;

class StudentEditCtrl
{

    private $form; //tworzymy formularz
    public $alert;
    private $records;
    private $passList;
    private $zmienna;
    private $strona;
    private $liczba;
    private $liczba2;
    private $limit;
    public function __construct()
    {   //tworzenie obiektu formularza
        $this->form = new StudentEditForm();
    }

    //walidacja:

    public function validateInput()
    { //1.Pobranie parametrów
        $this->form->Imie = getFromRequest('Imie');
        $this->form->Nazwisko = getFromRequest('Nazwisko');
        $this->form->Pesel = getFromRequest('Pesel');
        $this->form->ID_Modyfikatora = $_SESSION['ID'][0];
        $this->form->Data_Modyfikacji = date('Y.m.d');
        $this->form->Klasa = getFromRequest('Klasa');
        $this->form->Oddzial = getFromRequest('Oddzial');

       // if ( getMessages()->isError() ) return false;
        // 1. sprawdzenie czy wartości wymagane nie są puste
        if (empty(trim($this->form->Imie))) {
            $alert = 'Podaj imię';
            $response = $alert;
            exit($response);
            return false;
        }
        if (empty(trim($this->form->Nazwisko))) {
            $alert = 'Podaj nazwisko';
            $response = $alert;
            exit($response);
            return false;
        }
        if (empty(trim($this->form->Pesel))) {
            $alert = 'Podaj pesel';
            $response = $alert;
            exit($response);
            return false;
        }
        if (empty(trim($this->form->Klasa))) {
            $alert = 'Podaj klasę';
            $response = $alert;
            exit($response);
            return false;
        }
        if (empty(trim($this->form->Oddzial))) {
            $alert = 'Podaj Oddzial';
            $response = $alert;
            exit($response);
            return false;
        }

      //2. Walidacja
            if(!preg_match('/^[a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ]+$/', $this->form->Imie))
        {
            echo "<script type='text/javascript'>alert('Imię zawiera niepoprawne znaki');</script>";
            return false;
        }
        if(!preg_match('/^[a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ]+$/', $this->form->Nazwisko))
        {
            echo "<script type='text/javascript'>alert('Nazwisko zawiera niepoprawne znaki');</script>";
            return false;
        }
        if(!preg_match('/^[0-9]+$/', $this->form->Pesel))
        {
            echo "<script type='text/javascript'>alert('Pesel zawiera niepoprawne znaki');</script>";
            return false;
        }
        if(strlen($this->form->Pesel)!=11)
        {
            echo "<script type='text/javascript'>alert('Niepoprawna ilość cyfr w peselu');</script>";
            return false;
        }

        if(!ctype_digit($this->form->Klasa))
        {
            echo "<script type='text/javascript'>alert('Niepoprawna cyfra klasy');</script>";
            return false;
        }
        if(!preg_match("/^[0-9]{1}$/", $this->form->Klasa))
        {
            echo "<script type='text/javascript'>alert('Niepoprawna ilość cyfr klasy');</script>";
            return false;
        }
        $str = $this->form->Oddzial;
        if(!preg_match("/^[0-9]{1}[a-zA-Z]{1}$/", $str))
        {
            echo "<script type='text/javascript'>alert('Niepoprawny oddział');</script>";
            return false;
        }
        return true;

    }

    public function validateEdit()
    {
        $this->form->ID_Ucznia=getFromRequest('id', true, "błędne wywołanie aplikacji");
        return ! getMessages()->isError();
    }

    public function action_studentNew()
    {
        $this->generateView();
    }
//wysiweltenie rekordu do edycji wskazanego parametrem 'id'
    public function action_studentEdit()
    {
        if($this->validateEdit())
        {
            try
            {// 2. odczyt z bazy danych osoby o podanym ID (tylko jednego rekordu)
                $record = getDB()->get("uczen", "*",
                    [
                    "ID_Ucznia" => $this->form->ID_Ucznia
                    ]);
                // 2.1 jeśli osoba istnieje to wpisz dane do obiektu formularza
                $this->form->ID_Ucznia=$record['ID_Ucznia'];
                $this->form->Imie=$record['Imie'];
                $this->form->Nazwisko=$record['Nazwisko'];
                $this->form->Pesel=$record['Pesel'];
                $this->form->Klasa=$record['Klasa'];
                $this->form->Oddzial=$record['Oddzial'];
                getSmarty()->assign('form', $this->form);
            }
            catch(PDOException $e)
            {
                echo "<script type='text/javascript'>alert('Błąd gruby');</script>";
            }

        }
        // 3. Wygenerowanie widoku
        getSmarty()->display('StudentosEditos.tpl');
    }
    public function countReload()
    {
        try
        {
            $this->liczba2 = getDB()->count("uczen", [
                "ID_Ucznia"
            ],[
                "OR"=>
                    [
                        "Imie[~]" => $this->zmienna,
                        "Nazwisko[~]" => $this->zmienna,
                        "Pesel[~]" => $this->zmienna,
                        "Klasa[~]" => $this->zmienna,
                        "Oddzial[~]" => $this->zmienna,
                    ]
            ]);
        }
        catch(PDOException $e)
        {

        }
        return $this->liczba2;
    }
    public function action_reloadStudent()
    {
        $this->zmienna = explode(" ", getFromRequest('search'));
        $this->strona = getFromRequest('currentSite');
        if ($this->strona=="") $this->strona=1;
        $this->strona = $this->strona*5-5;
        $this->limit =5;
        $this->countReload();
        try
        {
            $this->records = getDB()->select("uczen", [
                "ID_Ucznia",
                "Imie",
                "Nazwisko",
                "Pesel",
                "Klasa",
                "Oddzial",
            ],[
                "OR"=>
                [
                    "Imie[~]" => $this->zmienna,
                    "Nazwisko[~]" => $this->zmienna,
                    "Pesel[~]" => $this->zmienna,
                    "Klasa[~]" => $this->zmienna,
                    "Oddzial[~]" => $this->zmienna,

                ],'LIMIT' => [$this->strona, 5]
            ]);
        }
        catch(PDOException $e)
        {

        }

        getSmarty()->assign('iloscStron', ceil($this->liczba2/5));
        getSmarty()->assign('iloscWynikow', $this->liczba2);
        getSmarty()->assign('lista', $this->records);
        getSmarty()->display('studentList.tpl');
    }


    public function action_studentDelete()
    {// 1. walidacja id osoby do usuniecia
        if($this->validateEdit())
        {
            try
            {// 2. usunięcie rekordu
                getDB()->delete("uczen", [
                    "ID_Ucznia" => $this->form->ID_Ucznia

            ]);

                echo "<script type='text/javascript'>alert('Usunięto pomyślnie');</script>";

            }   catch (PDOException $e)
                {
                    echo "<script type='text/javascript'>alert('nie da się i ni mo');</script>";
                }
        }
        // 3. Przekierowanie na stronę główną
        forwardTo('studentList');
    }

    public function action_studentSave()
    {// 1. Walidacja danych formularza (z pobraniem)
        $tempPass = mt_rand(41029374, 92718465);
        if ($this->validateInput()) {// 2. Zapis danych w bazie
            try {
                if (!getDB()->has("uczen", ['Pesel' => $this->form->Pesel])) {
                    //2.1 Nowy rekord
                    getDB()->insert("uczen", [
                        "Imie" => $this->form->Imie,
                        "Nazwisko" => $this->form->Nazwisko,
                        "Pesel" => $this->form->Pesel,
                        "Haslo" => $tempPass,
                        "ID_Modyfikatora" => $_SESSION['ID'][0],
                        "Data_Modyfikacji" => $this->form->Data_Modyfikacji = date('Y.m.d'),
                        "Klasa" => $this->form->Klasa,
                        "Oddzial" => $this->form->Oddzial,
                        "Tymczasowe" => 1
                    ]);
                    $alert = "dodano pomyślnie";
                    $response = $alert;

                    exit($response);

                } else {
                    $alert = "Taki Pesel już istnieje";
                    $response = $alert;
                    exit($response);
                }
                getMessages()->addInfo('Pomyślnie zapisano rekord');
            } catch (PDOException $e) {
                getMessages()->addError('Wystąpił nieoczekiwany błąd podczas zapisu rekordu');
                if (getConf()->debug) getMessages()->addError($e->getMessage());
            }
            // 3b. Po zapisie przejdź na stronę listy osób (w ramach tego samego żądania http)
            forwardTo('librus');
        } else {
            // 3c. Gdy błąd walidacji to pozostań na stronie
            //echo "<script>alert('$this->form');</script>";
            $this->generateView();
        }
    }

        public function action_StudentList()
    {
            try
            {
                $this->liczba = getDB()->count("uczen",[]);
            }
            catch(PDOException $e)
            {

            }
        getSmarty()->assign('iloscStron', ceil(($this->liczba)/5));
        getSmarty()->assign('iloscWynikow', ($this->liczba));
        try
        {
            $this->records = getDB()->select("uczen", [
                "ID_Ucznia",
                "Imie",
                "Nazwisko",
                "Pesel",
                "Klasa",
                "Oddzial",
            ],
                [
                    'LIMIT' => [0, 5]
                ]);
        }
        catch(PDOException $e)
        {

        }

        getSmarty()->assign('lista', $this->records);
        $this->generateView();

    }

    public function action_TemporaryPass()
    {
        try
        {
            $this->passList = getDB()->select("uczen", [
                "Imie",
                "Nazwisko",
                "Pesel",
                "Haslo"
            ],[
               "Tymczasowe" => 1
            ]);
        }
        catch(PDOException $e)
        {

        }
        getSmarty()->assign('hasla', $this->passList);
        getSmarty()->display('TemporaryPass.tpl');
    }

    public function action_studentUpdate()
    {
        if ($this->validateInput())
        {
            if($this->validateEdit())
            {

                try
                {
                    //2.1 Nowy rekord
                    getDB()->update("uczen", [
                        "Imie" => $this->form->Imie,
                        "Nazwisko" => $this->form->Nazwisko,
                        "Pesel" => $this->form->Pesel,
                        "ID_Modyfikatora" => $_SESSION['ID'][0],
                        "Data_Modyfikacji" => $this->form->Data_Modyfikacji = date('Y.m.d'),
                        "Klasa" => $this->form->Klasa,
                        "Oddzial" => $this->form->Oddzial
                    ], [
                        "ID_Ucznia" =>$this->form->ID_Ucznia
                    ]);
                }
                catch(PDOException $e)
                {
                    getMessages()->addError('Wystąpił nieoczekiwany błąd podczas zapisu rekordu');
                    if (getConf()->debug) getMessages()->addError($e->getMessage());
                }
            }
        }
        else {
            // 3c. Gdy błąd walidacji to pozostań na stronie
            //echo "<script>alert('$this->form');</script>";
            //$alert = "błąd";
            //$response = $alert;

            //exit($response);
            echo "błąd";
        }
    }



    public function generateView()
    {
        getSmarty()->assign('form',$this->form); // dane formularza dla widoku
        getSmarty()->display('StudentEdit.tpl');
    }
}