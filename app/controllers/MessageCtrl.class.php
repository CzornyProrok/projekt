<?php

namespace app\controllers;

use app\forms\MessageForm;
use PDOException;

class MessageCtrl
{
    private $form;

    public function __construct()
    {
        $this->form = new MessageForm();
    }
    public function action_showMessageUser()
    {
        $this->form->ID_Ucznia = getFromRequest('id');

        try
        {
            $this->form = getDB()-> select("wiadomosc",[
                    "[><]pracownik" => ["ID_Pracownika" => "ID_Pracownika"],
                    "[><]uczen" => ["ID_Ucznia" => "ID_Ucznia"],
                ],[
                    "pracownik.Imie(ImiePracownika)",
                    "pracownik.Nazwisko(NazwiskoPracownika)",
                    "wiadomosc.Wiadomosc",
                    "wiadomosc.Data_Wiadomosci",
                    "uczen.Imie(ImieUcznia)",
                    "uczen.Nazwisko(NazwiskoUcznia)"
                ],[
                "ORDER" => ["wiadomosc.Data_Wiadomosci" => "DESC"],
                    "wiadomosc.ID_Ucznia" => $this->form->ID_Ucznia
            ]);
        }
        catch(PDOException $e)
        {
            echo "błąd odczytywania wiadomości";
        }
        getSmarty()->assign('wiadomosc', $this->form);
        getSmarty()->display('message.tpl');
    }

    public function action_showMessageAdmin()
    {
        $this->form->ID = getFromRequest('id');

        try
        {
            $this->form = getDB()-> select("wiadomosc",[
                "[><]pracownik" => ["ID_Pracownika" => "ID_Pracownika"],
                "[><]uczen" => ["ID_Ucznia" => "ID_Ucznia"],
            ],[
                "pracownik.Imie(ImiePracownika)",
                "pracownik.Nazwisko(NazwiskoPracownika)",
                "wiadomosc.Wiadomosc",
                "wiadomosc.Data_Wiadomosci",
                "uczen.Imie(ImieUcznia)",
                "uczen.Nazwisko(NazwiskoUcznia)"
            ],[
                "ORDER" => ["wiadomosc.Data_Wiadomosci" => "DESC"],
                "wiadomosc.ID_Pracownika" => $this->form->ID
            ]);
        }
        catch(PDOException $e)
        {
            echo "błąd odczytywania wiadomości";
        }
        getSmarty()->assign('wiadomosc', $this->form);
        getSmarty()->display('message.tpl');
    }

    public function action_addMessage()
    {
        $this->form->ID_Ucznia = getFromRequest('id');
        $this->form->Wiadomosc = getFromRequest('wiadomosc');
        try
        {
            getDB()->insert("wiadomosc",
                [
                    "ID_Ucznia" => $this->form->ID_Ucznia,
                    "ID_Pracownika" => $_SESSION['ID'][0],
                    "wiadomosc" => $this->form->Wiadomosc
                ]);
        }
        catch(PDOException $e)
        {
            echo "bląd dodawania";
        }
    }
}