$(document).ready(function(){
    'use strict';
    //tu trafia dalszy nasz dalszy kod
    const Imie = document.getElementsByName('Imie')[0];
    const Nazwisko = document.getElementsByName('Nazwisko')[0];
    const Pesel = document.getElementsByName('Pesel')[0];
    const Klasa = document.getElementsByName('Klasa')[0];
    const Oddzial = document.getElementsByName('Oddzial')[0];
    const ID = document.getElementsByName('id')[0];
    const formAlert = document.querySelector(".emailFormAlert");


    $('.formSubmit2').on('click', function (event) {

        event.preventDefault();
        //reszta
        if(validateForm()){
            //tu pojawi się AJAX

            $.ajax({
                type: "POST",
                url: 'ctrl.php?action=studentUpdate',
                data: {
                    'Imie' : Imie.value,
                    'Nazwisko' : Nazwisko.value,
                    'Pesel' : Pesel.value,
                    'Klasa' : Klasa.value,
                    'Oddzial' : Oddzial.value,
                    'id' : ID.value
                },
                success: function (data) {
                    if(validateForm)
                    {
                        Imie.value='';
                        Nazwisko.value='';
                        Pesel.value='';
                        Klasa.value='';
                        Oddzial.value='';
                        // alert(data);
                        window.location.href = 'ctrl.php?action=studentList';}
                }

            });
            // sendEmail.fail(function(response) {
            //     formAlert.innerHTML='Coś poszło nie tak :( '+response.responseText;
            //
            // });
            // sendEmail.done(function(response){
            //     formAlert.innerHTML='okej, '+
            //
            //
            // });
        }
    });
    function Validate(val)
    {

        if (!val.match(/^[a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ]+$/))
        {
            return false;
        }

        return true;
    }

    function validateForm()
    {

        if(Imie.validity.valueMissing)
        {

            markWrongInput(Imie, "Podaj imię");
            return false;

        }
        else if(!Imie.validity.valueMissing)
        {

            if(!Validate(Imie.value))
            {
                markWrongInput(Imie,"Podaj poprawne imię!");
                return false;
            }
        }
        if(Validate(Imie.value))
        {
            if (Nazwisko.validity.valueMissing)
            {
                markWrongInput(Nazwisko, "Podaj nazwisko");
                return false;
            }
            else if (!Nazwisko.validity.valueMissing)
            {

                if(!Validate(Nazwisko.value))
                {
                    markWrongInput(Nazwisko,"Podaj poprawne nazwisko!");
                    return false;
                }
            }
        }

        if(Validate(Nazwisko.value))
        {
            if (Pesel.validity.valueMissing){
                markWrongInput(Pesel,"Podaj pesel");
                return false;
            }
            else if(!Pesel.validity.valueMissing)
            {
                var str=Pesel.value.length;
                if(str!=11)
                {
                    markWrongInput(Pesel, "Liczba cyfr w numerze PESEL nie prawidłowa");
                    return false;
                }
                if (!Pesel.value.match(/^[0-9]+$/))
                {
                    markWrongInput(Pesel, "PESEL zawiera inne znaki niż cyfry");
                    return false;
                }
            }
        }

        if(Pesel.value.length==11 && Pesel.value.match(/^[0-9]+$/))
        {
            if(Klasa.validity.valueMissing)
            {
                markWrongInput(Klasa, "Podaj numer klasy");
                return false;
            }
            else if(!Klasa.validity.valueMissing)
            {
                if(Klasa.value.length!=1)
                {
                    markWrongInput(Klasa, "nieprawidłowa ilość znaków w klasie");
                    return false;
                }
                if(!Klasa.value.match(/^[0-9]+$/))
                {
                    markWrongInput(Klasa, "Podano nieprawidłową cyfrę");
                    return false;
                }

            }
        }
        if(Klasa.value.length==1 && Klasa.value.match(/^[0-9]+$/))
        {
            if(Oddzial.validity.valueMissing)
            {
                markWrongInput(Oddzial, "Podaj oddział");
                return false;
            }
            else if(!Oddzial.validity.valueMissing)
            {
                if(!Oddzial.value.length==2)
                {
                    markWrongInput(Oddzial, "Nieprawidłowa ilość znaków w oddziale");
                    return false;
                }
                var tab=Oddzial.value.split('');
                if(!tab[0].match(/^[0-9]+$/))
                {
                    markWrongInput(Oddzial, "Pierwszy znak nie jest cyfrą");
                    return false;
                }
                if(!tab[1].match(/^[a-zA-z]+$/))
                {
                    markWrongInput(Oddzial, "Drugi znak nie jest literą");
                    return false;
                }
            }
        }
        if(Oddzial.value.length==2 && tab[0].match(/^[0-9]+$/) && tab[1].match(/^[a-zA-z]+$/) )
        {
            return true;
        }

    }
    function markWrongInput(wrongElement,alert){
        formAlert.innerHTML=alert;
        wrongElement.classList.add('wrongInput');
        wrongElement.addEventListener("focus", function (){this.classList.remove('wrongInput')});
    }

});