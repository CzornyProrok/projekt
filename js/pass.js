$(function(){
    'use strict';
    //tu trafia dalszy nasz dalszy kod
    const newPass = document.getElementsByName('newPass')[0];
    const newPassR = document.getElementsByName('newPassR')[0];
    const formAlert = document.querySelector(".emailFormAlert");


    $('.formSubmit3').on('click', function (event) {

        event.preventDefault();
        //reszta
        if(validateForm()){
            //tu pojawi się AJAX

            $.ajax({
                type: "POST",
                url: 'ctrl.php?action=passChangeDo',
                data: {
                    'newPass' : newPass.value,
                    'newPassR' : newPassR.value,
                },
                success: function (data)
                {
                    if (validateForm)
                    {
                        alert(data);
                        window.location.href = 'ctrl.php?action=librus';
                    }

                }
            })
        }
    });

    function Validate(val)
    {

        if (!val.match(/^[a-zA-ZąćęłńóśźżĄĘŁŃÓŚŹŻ0-9]+$/))
        {
            return false;
        }

        return true;
    }

    function validateForm()
    {

        if(newPass.validity.valueMissing)
        {
            markWrongInput(newPass, "Podaj nowe hasło");
            return false;
        }
        else if(newPassR.validity.valueMissing)
        {
            markWrongInput(newPassR, "Nie powtórzono hasła")
            return false;
        }

        if(newPass.value != newPassR.value)
        {
            markWrongInput(newPassR, "Hasło rózni się od pierwszego hasła")
            return false;
        }

        if(newPass.value.length<8)
        {
            markWrongInput(newPass, "Hasło jest za krótkie")
            return false;
        }

        if(!Validate(newPass.value))
        {
            markWrongInput(newPass, "Podane hasło zawiera niepoprawne znaki. Hasło może zawierać litery i cyfry")
            return false;
        }
        return true

    }
    function markWrongInput(wrongElement,alert){
        formAlert.innerHTML=alert;
        wrongElement.classList.add('wrongInput');
        wrongElement.addEventListener("focus", function (){this.classList.remove('wrongInput')});
    }

});