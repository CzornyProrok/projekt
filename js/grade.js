$(function(){
    'use strict';
    //tu trafia dalszy nasz dalszy kod
    const Ocena = document.getElementsByName('Ocena')[0];
    const Komentarz = document.getElementsByName('Komentarz')[0];
    const ID_Ucznia = document.getElementsByName('ID_Ucznia')[0];
    const formAlert = document.querySelector(".emailFormAlert");


    $('.formSubmit4').on('click', function (event) {

        event.preventDefault();
        //reszta
        if(validateForm()){
            //tu pojawi się AJAX

            $.ajax({
                type: "POST",
                url: 'ctrl.php?action=addGrade',
                data: {
                    'Ocena' : Ocena.value,
                    'Komentarz' : Komentarz.value,
                    'ID_Ucznia' : ID_Ucznia.value,
                },
                success: function (data) {
                    if(validateForm)
                    { //contactForm.fadeOut();
                        Ocena.value='';
                        Komentarz.value='';
                        window.location.href = 'ctrl.php?action=showGrades&id='+ID_Ucznia.value;}

                }

            });
            // sendEmail.fail(function(response) {
            //     formAlert.innerHTML='Coś poszło nie tak :( '+response.responseText;
            //
            // });
            // sendEmail.done(function(response){
            //     formAlert.innerHTML='okej, '+
            //
            //
            // });
        }
    });


    function validateForm()
    {

        if (Ocena.validity.valueMissing) {

            markWrongInput(Ocena, "Podaj ocenę");
            return false;

        }
        else if (!Ocena.validity.valueMissing)
        {

            if(Ocena.value.length>3 || Ocena.value.length<1)
            {
                markWrongInput(Ocena, "Podaj poprawną ilość znaków oceny");
                return false;
            }
            var $arr=Ocena.value;
            if(Ocena.value.length==1 && isNaN($arr[0]))
            {
                markWrongInput(Ocena, "Podaj poprawnie ocenę");
                return false;
            }
            else if(Ocena.value.length==2 && isNaN($arr[0]))
            {
                markWrongInput(Ocena, "Podaj poprawnie ocenę");
                return false;
            }
            else if(Ocena.value.length==2 && !isNaN($arr[1]))
            {
                markWrongInput(Ocena, "Podaj poprawnie ocenę");
                return false;
            }
            else if(Ocena.value.length==2 && !$arr[1].match(/^[-+]+$/))
            {
                markWrongInput(Ocena, "Podaj poprawnie ocenę");
                return false;
            }
            else if(Ocena.value.length==3 && isNaN($arr[0]))
            {
                markWrongInput(Ocena, "Podaj poprawnie ocenę");
                return false;
            }

            else if(Ocena.value.length==3 && !isNaN($arr[1]))
            {
                markWrongInput(Ocena, "Podaj poprawnie ocenę");
                return false;
            }

            else if(Ocena.value.length==3 && !isNaN($arr[2]))
            {
                markWrongInput(Ocena, "Podaj poprawnie ocenę");
                return false;
            }
            else if(Ocena.value.length==3 && !$arr[1].match(/^[-]+$/) && !$arr[2].match(/^[-]+$/))
            {
                markWrongInput(Ocena, "Podaj poprawnie ocenę");
                return false;
            }

        }
        if (Komentarz.validity.valueMissing)
        {
                markWrongInput(Komentarz, "Podaj opis oceny");
                return false;
        }
        else if(!Komentarz.validity.valueMissing) return true;

    }

    function markWrongInput(wrongElement,alert){
        formAlert.innerHTML=alert;
        wrongElement.classList.add('wrongInput');
        wrongElement.addEventListener("focus", function ()
        {this.classList.remove('wrongInput')});
    }

});