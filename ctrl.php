<?php
require_once 'init.php';
// Rozszerzenia w aplikacji bazodanowej:
// - nowe pola dla konfiguracji połączenia z bazą danych w klasie Config
// - inicjalizacja połączenia z bazą w skrypcie init.php, za pomocą funkcji getDB() - podobnie jak dla wcześniejszych obiektów

// Do połączenia z bazą danych wykorzystujemy "maleńką" bibliotekę Medoo, która obudowuje standardowy obiekt PDO za pomocą klasy Medoo.
// Biblioteka Medoo ułatwia dostęp do bazy dla większości standardowych rodzajów zapytań, przez brak konieczności używania SQL'a.

// Jeżeli użytkownik chce jednak używać bezpośrednio PDO, to biblioteki używamy jedynie w celu połączenia z bazą, a później
// pobieramy obiekt PDO po połączeniu (metoda pdo() obiektu klasy Medoo).

getRouter()->setDefaultRoute('librus'); // akcja/ścieżka domyślna
getRouter()->setLoginRoute('login'); // akcja/ścieżka na potrzeby logowania (przekierowanie, gdy nie ma dostępu)

getRouter()->addRoute('librus',        'LibrusCtrl', ['user', 'admin']);
getRouter()->addRoute('loginShow',		'LoginCtrl');
getRouter()->addRoute('login',			'LoginCtrl');
getRouter()->addRoute('logout',			'LoginCtrl');
getRouter()->addRoute('studentNew',		'StudentEditCtrl',	['admin']);
getRouter()->addRoute('studentEdit',	'StudentEditCtrl',	['admin']);
getRouter()->addRoute('studentSave',	'StudentEditCtrl',	['admin']);
getRouter()->addRoute('studentDelete',	'StudentEditCtrl',	['admin']);
getRouter()->addRoute('studentList',	'StudentEditCtrl',	['admin']);
getRouter()->addRoute('TemporaryPass',	'StudentEditCtrl',	['admin']);
getRouter()->addRoute('studentUpdate', 'StudentEditCtrl', ['admin']);
getRouter()->addRoute('passChange',    'passChangeCtrl',  ['user']);
getRouter()->addRoute('passChangeDo',  'passChangeCtrl',  ['user']);
getRouter()->addRoute('reloadGrades',  'GradeCtrl',       ['admin']);
getRouter()->addRoute('addGrade',      'GradeCtrl',       ['admin']);
getRouter()->addRoute('showGrades',    'GradeCtrl',       ['admin']);
getRouter()->addRoute('showGradesUser',    'GradeCtrl',       ['user']);
getRouter()->addRoute('deleteGrade',   'GradeCtrl',       ['admin']);
getRouter()->addRoute('editGrade',     'GradeCtrl',       ['admin']);
getRouter()->addRoute('updateGrade',     'GradeCtrl',       ['admin']);
getRouter()->addRoute('reloadStudent', 'StudentEditCtrl',       ['admin']);
getRouter()->addRoute('addMessage', 'MessageCtrl',       ['admin']);
getRouter()->addRoute('showMessageUser', 'MessageCtrl',       ['user']);
getRouter()->addRoute('showMessageAdmin', 'MessageCtrl',       ['admin']);

getRouter()->go();