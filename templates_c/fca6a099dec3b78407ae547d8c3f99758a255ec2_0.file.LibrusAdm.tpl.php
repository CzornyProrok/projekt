<?php
/* Smarty version 3.1.30, created on 2018-09-27 10:59:02
  from "C:\xampp\htdocs\projekt\app\views\LibrusAdm.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bac9bd676dad6_20338488',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fca6a099dec3b78407ae547d8c3f99758a255ec2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\LibrusAdm.tpl',
      1 => 1538038725,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bac9bd676dad6_20338488 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE HTML>

<html lang="pl">
<head>
    <title>Librus</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/css/main.css" />
    <noscript><link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/css/noscript.css" /></noscript>
</head>
<body class="is-preload">
<!-- Page Wrapper -->
<div id="page-wrapper">

    <!-- Header -->
    <header id="header" class="alt">
        <h1><a href="index.html">Librus</a></h1>
        <nav>
            <a href="#menu">Menu</a>
            <a href="#menu2">Wyloguj</a>
        </nav>
    </header>

    <!-- Menu -->
    <nav id="menu">
        <div class="inner">
            <h2>Menu ocen</h2>
            <ul class="links">
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
studentList">Rejestracja oraz edycja</a></li>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
TemporaryPass">Paski do druku</a></li>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
showMessageAdmin&id=<?php echo $_smarty_tpl->tpl_vars['sesID']->value;?>
">Wysłane wiadomości</a></li>
            </ul>
        </div>
    </nav>
    <nav id="menu2">
        <div class="inner">
            <h2>Wyloguj</h2>
            <div class="links">
                <p>Czy na pewno chcesz się wylogować?</p>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
logout">Wyloguj</a></li>
                    </div>
            </div>
    </nav>
    <!-- Banner -->
    <section id="banner">
        <div class="inner">
            <div class="logo"><span class="icon fa-diamond"></span></div>
            <h2>Witamy w Librusie</h2>
            <p>Najlepsze narzędzie do uprzykrzania życia Twojemu dziecku! </p>
        </div>
    </section>



    <!-- Footer -->
    <section id="footer">
        <div class="inner">
            <h2 class="major">Błąd na stronie?</h2>
            <p>Zgłoś administratorowi!</p>
            <ul class="contact">
                <li class="fa-home">
                    Kompania Braci<br />
                    ul. Drogowa 40<br />
                    Jaworzno, 43-600
                </li>
                <li class="fa-phone">511-420-778</li>
                <li class="fa-envelope"><a href="#">maciejkna470@gmail.com</a></li>
                <li class="fa-twitter"><a href="#">no twitter</a></li>
                <li class="fa-facebook"><a href="#">no facebook</a></li>
                <li class="fa-instagram"><a href="#">no instagram</a></li>
            </ul>
            <ul class="copyright">
                <li>&copy; Untitled Inc. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
            </ul>
        </div>
    </section>

</div>

<!-- Scripts -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.scrollex.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/browser.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/breakpoints.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/util.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/main.js"><?php echo '</script'; ?>
>

</body>
</html><?php }
}
