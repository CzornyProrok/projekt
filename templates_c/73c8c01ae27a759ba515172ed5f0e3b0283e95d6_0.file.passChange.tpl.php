<?php
/* Smarty version 3.1.30, created on 2018-09-26 13:28:03
  from "C:\xampp\htdocs\projekt\app\views\passChange.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bab6d43ac92f6_05167423',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '73c8c01ae27a759ba515172ed5f0e3b0283e95d6' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\passChange.tpl',
      1 => 1537262881,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bab6d43ac92f6_05167423 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE HTML>
<html lang = 'pl'>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/css/main.css" />
</head>
<body>
<div id="contact4" class="contact4">
    <form class="form"  method = "post" action = "<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
passChangeDo">
        <div class = "fields">
            <div class = "field">
                <label>Nowe hasło:</label>
                <input type="password" name="newPass" placeholder="Nowe hasło(musi mieć przynajmniej 8 znaków, nie może zawierać znaków specjalnych, w tym spacji)" id="newPass"  required/>
            </div>

            <div class = "field">
                <label>Powtórz nowe hasło:</label>
                <input type="password" name="newPassR" placeholder="Powtórz nowe hasło" id="newPassR" required/>
            </div>
                <button class="formSubmit3" name="submit" type="submit">Zatwierdź</button>
            </div>
        </div>
        <div class="emailFormAlert"></div>
    </form>

</div>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.scrollex.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/browser.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/breakpoints.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/util.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/pass.js"><?php echo '</script'; ?>
>
</body>
</html><?php }
}
