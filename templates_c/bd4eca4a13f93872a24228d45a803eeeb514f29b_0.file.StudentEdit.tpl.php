<?php
/* Smarty version 3.1.30, created on 2018-09-26 12:41:26
  from "C:\xampp\htdocs\projekt\app\views\StudentEdit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bab62566a5029_49516820',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bd4eca4a13f93872a24228d45a803eeeb514f29b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\StudentEdit.tpl',
      1 => 1537958484,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:studentList.tpl' => 1,
  ),
),false)) {
function content_5bab62566a5029_49516820 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE HTML>
<html lang="pl">

<head>
    <title>Rejestracja</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/css/main.css" />
   <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"> -->
    <noscript><link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/css/noscript.css" /></noscript>
</head>

<body class="is-preload">
<!-- Page Wrapper -->
<div id="page-wrapper">

    <!-- Header -->
    <header id="header" class="alt">
        <h1><a href="#">Uczniowie</a></h1>
        <nav>
            <button id="open-contact-btn2" class="about-link">
                <i class="fa fa-envelope-o" aria-hidden="true"></i> Formularz rejestracji
            </button>
            <div id="contact2" class="contact2">
                        <form id ="form2" class="form"  method = "post" action = "<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
studentSave">
                            <div class = "fields">
                                <div class = "field">
                                    <input type="text" name="Imie" placeholder="Imie" id="Imie" required/>
                                </div>
                                <div class = "field">
                                    <input type="text" name="Nazwisko" placeholder="Nazwisko" id="Nazwisko" required/>
                                </div>
                                <div class = "field">
                                    <input type="text" name="Pesel" placeholder="PESEL" id="Pesel" required/>
                                </div>
                                <div class = "field">
                                    <input type="text" name="Klasa" placeholder="Klasa (np. 2)" id="Klasa" required/>
                                </div>
                                <div class = "field">
                                    <input type="text" name="Oddzial" placeholder="Oddział (np. 2A)" id="Oddzial" required/>
                                </div>
                                <div class="field">
                                <button class="formSubmit" name="submit" type="submit">Utwórz</button>
                                    <button type="button" id="close-contact-btn2" class="close-btn">
                                        <i class="fa fa-times close-form" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="emailFormAlert"></div>
                        </form>

            </div>
            <div class="button"><a href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
librus">Strona główna</a><div>
        </nav>
    </header>

    <!-- Banner -->
    <section id="banner">
        <div class="inner">
            <div class="logo"><span class="icon fa-diamond"></span></div>
            <h2>Witamy w panelu</h2>
            <p>Wyedytuj dane, dodaj lub usuń ucznia.</p>
        </div>
    </section>
    <section id="four" class="wrapper alt style1">
        <div class="inner">
            <h2 class="major">Lista uczniów</h2>
                <input id="search" type="text" placeholder="Wyszukaj..." onkeyup="ajaxPostForm('<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
reloadStudent', 'studentContainer');">
            <div class="studentContainer" id="studentContainer">
                <?php $_smarty_tpl->_subTemplateRender("file:studentList.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>

        </div>
            </section>
        </div>
    </section>
    <!-- Footer -->
    <section id="footer">
        <div class="inner">
            <h2 class="major">Błąd na stronie?</h2>
            <p>Zgłoś administratorowi!</p>
            <ul class="contact">
                <li class="fa-home">
                    Kompania Braci<br />
                    ul. Drogowa 40<br />
                    Jaworzno, 43-600
                </li>
                <li class="fa-phone">511-420-778</li>
                <li class="fa-envelope"><a href="#">maciejkna470@gmail.com</a></li>
                <li class="fa-twitter"><a href="#">no twitter</a></li>
                <li class="fa-facebook"><a href="#">no facebook</a></li>
                <li class="fa-instagram"><a href="#">no instagram</a></li>
            </ul>
            <ul class="copyright">
                <li>&copy; Untitled Inc. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
            </ul>
        </div>
    </section>

</div>

<!-- Scripts -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.scrollex.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/browser.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/breakpoints.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/util.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/main.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/lista2.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/paginator.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/deleting.js"><?php echo '</script'; ?>
>
</body>
</html><?php }
}
