<?php
/* Smarty version 3.1.30, created on 2018-09-25 10:08:09
  from "C:\xampp\htdocs\projekt\app\views\gradesList.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ba9ece9f18564_85883785',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3452d3dddb67a88ae9690d380912ece9a5e2a2cf' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\gradesList.tpl',
      1 => 1537862522,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ba9ece9f18564_85883785 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table id="table">
    
    <tr>
        <th>Ocena</th>
        <th>Wystawił(a)</th>
        <th>Data modyfikacji</th>
        <th>Modyfikował(a) ostatnio</th>
        <th>Opis</th>
    </tr>
    
    
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lista']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
        <tr><th><?php echo $_smarty_tpl->tpl_vars['l']->value['Ocena'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['l']->value['ImieWystawcy'];?>
 <?php echo $_smarty_tpl->tpl_vars['l']->value['NazwiskoWystawcy'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['l']->value['Data_Modyfikacji'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['l']->value['ImieEdytora'];?>
 <?php echo $_smarty_tpl->tpl_vars['l']->value['NazwiskoEdytora'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['l']->value['Opis'];?>
</th><td><a class="button" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
editGrade&idOceny=<?php echo $_smarty_tpl->tpl_vars['l']->value['ID_Oceny'];?>
">Edytuj</a><a class="button" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
deleteGrade&idOceny=<?php echo $_smarty_tpl->tpl_vars['l']->value['ID_Oceny'];?>
&id=<?php echo $_smarty_tpl->tpl_vars['id']->value[0];?>
" onclick="return deleteFunction()">Usuń</a></td></tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


    
</table><?php }
}
