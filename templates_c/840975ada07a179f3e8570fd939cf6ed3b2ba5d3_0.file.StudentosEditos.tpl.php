<?php
/* Smarty version 3.1.30, created on 2018-09-24 11:13:57
  from "C:\xampp\htdocs\projekt\app\views\StudentosEditos.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ba8aad5df4505_28340525',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '840975ada07a179f3e8570fd939cf6ed3b2ba5d3' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\StudentosEditos.tpl',
      1 => 1537780437,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ba8aad5df4505_28340525 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE HTML>
<html lang = 'pl'>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/css/main.css" />
</head>
<body>
<div id="contact3" class="contact3">
    <form class="form"  method = "post" action = "<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
studentUpdate">
        <div class = "fields">
            <div class = "field">
                <label>Imię</label>
                <input type="text" name="Imie" placeholder="Imie" id="Imie" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->Imie;?>
" required/>
            </div>
            <div class = "field">
                <label>Nazwisko</label>
                <input type="text" name="Nazwisko" placeholder="Nazwisko" id="Nazwisko" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->Nazwisko;?>
" required/>
            </div>
            <div class = "field">
                <label>PESEL</label>
                <input type="text" name="Pesel" placeholder="PESEL" id="Pesel" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->Pesel;?>
" required/>
            </div>
            <div class = "field">
                <label>Klasa</label>
                <input type="text" name="Klasa" placeholder="Klasa (np. 2)" id="Klasa" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->Klasa;?>
" required/>
            </div>
            <div class = "field">
                <label>Oddział</label>
                <input type="text" name="Oddzial" placeholder="Oddział (np. 2A)" id="Oddzial" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->Oddzial;?>
" required/>
            </div>
            <div class = "field">
                <input type="hidden" name="id" id="id" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->ID_Ucznia;?>
">
            </div>
            <div class="field">
                <button class="formSubmit2" name="submit" type="submit">Zatwierdź</button>
                <a class="button" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
studentList">Anuluj</a>
            </div>

        </div>
        <div class="emailFormAlert"></div>
    </form>

</div>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.scrollex.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/browser.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/breakpoints.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/util.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/update.js"><?php echo '</script'; ?>
>
</body>
</html><?php }
}
