<?php
/* Smarty version 3.1.30, created on 2018-09-27 10:54:05
  from "C:\xampp\htdocs\projekt\app\views\gradesUser.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bac9aad7301a7_65655499',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '13ec9108b27f2d48d9277fbe16996a01cf98fa6a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\gradesUser.tpl',
      1 => 1538038444,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bac9aad7301a7_65655499 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <title>Librus</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/css/main.css" />
    <noscript><link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/css/noscript.css" /></noscript>
</head>
<body class = "is-preload">
<a class="button" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
librus" style="float: right">Strona główna</a>
<h2>LISTA OCEN</h2>
<table id="table">
    
    <tr>
        <th>Ocena</th>
        <th>Wystawił(a)</th>
        <th>Data modyfikacji</th>
        <th>Modyfikował(a) ostatnio</th>
        <th>Przedmiot</th>
        <th>Opis</th>
    </tr>
    
    
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lista']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
        <tr><th><?php echo $_smarty_tpl->tpl_vars['l']->value['Ocena'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['l']->value['ImieWystawcy'];?>
 <?php echo $_smarty_tpl->tpl_vars['l']->value['NazwiskoWystawcy'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['l']->value['Data_Modyfikacji'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['l']->value['ImieEdytora'];?>
 <?php echo $_smarty_tpl->tpl_vars['l']->value['NazwiskoEdytora'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['l']->value['Przedmiot'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['l']->value['Opis'];?>
</th></tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


    
</table>
</body>
</html><?php }
}
