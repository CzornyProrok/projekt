<?php
/* Smarty version 3.1.30, created on 2018-09-25 11:21:24
  from "C:\xampp\htdocs\projekt\app\views\GradosEditos.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ba9fe148853b5_35146848',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '21fef3593e8fe1ba99632fdee342eea04622f10d' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\GradosEditos.tpl',
      1 => 1537867278,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ba9fe148853b5_35146848 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE HTML>
<html lang = 'pl'>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/css/main.css" />
</head>
<body>
<div id="contact3" class="contact3">
    <form class="form"  method = "post" action = "<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
updateGrade">
        <div class = "fields">
            <div class = "field">
                <label>Ocena</label>
                <input type="text" name="Ocena" placeholder="Ocena" id="Ocena" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->Ocena;?>
" required/>
            </div>
            <div class = "field">
                <label>Komentarz</label>
                <input type="text" name="Komentarz" placeholder="Komentarz" id="Komentarz" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->Komentarz;?>
" required/>
            </div>
            <div class = "field">
                <input type="hidden" name="id" id="id" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->ID_Ucznia;?>
">
            </div>
            <div class = "field">
                <input type="hidden" name="idOceny" id="idOceny" value="<?php echo $_smarty_tpl->tpl_vars['form']->value->ID_Oceny;?>
">
            </div>
            <div class="field">
                <button class="formSubmit5" name="submit" type="submit">Zatwierdź</button>
                <a class="button" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
showGrades&id=<?php echo $_smarty_tpl->tpl_vars['form']->value->ID_Ucznia;?>
">Wróc do listy ocen</a>
            </div>
        </div>
        <div class="emailFormAlert"></div>
    </form>

</div>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.scrollex.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/browser.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/breakpoints.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/util.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/updateGrade.js"><?php echo '</script'; ?>
>
</body>
</html><?php }
}
