<?php
/* Smarty version 3.1.30, created on 2018-09-27 10:43:29
  from "C:\xampp\htdocs\projekt\app\views\LibrusUsr.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bac98312e4740_21651141',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b89019ef78bfacc6dc5196f7c3ae2806994ecbd2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\LibrusUsr.tpl',
      1 => 1538037808,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bac98312e4740_21651141 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <title>Librus</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/css/main.css" />
    <noscript><link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/css/noscript.css" /></noscript>
</head>
<body class = "is-preload">
<!-- Page Wrapper -->
<div id="page-wrapper">

    <!-- Header -->
    <header id="header" class="alt">
        <h1><a href="index.html">Librus</a></h1>
        <nav>
            <a href="#menu">Menu</a>
            <a href="#menu2">Wyloguj</a>
        </nav>
    </header>

    <!-- Menu -->
    <nav id="menu">
        <div class="inner">
            <h2>Menu ocen</h2>
            <ul class="links">
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
showGradesUser&id=<?php echo $_smarty_tpl->tpl_vars['sesID']->value;?>
">Lista ocen</a></li>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
showMessageUser&id=<?php echo $_smarty_tpl->tpl_vars['sesID']->value;?>
">Wiadomości</a></li>
                <li><a href="elements.html">soon</a></li>
            </ul>
        </div>
    </nav>
    <nav id="menu2">
        <div class="inner">
            <h2>Wyloguj</h2>
            <div class="links">
                <p>Czy na pewno chcesz się wylogować?</p>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
logout">Wyloguj</a></li>
            </div>
        </div>
    </nav>
</div>
<!-- Wrapper -->
<section id="wrapper">


    <!-- Four -->
    <section id="four" class="wrapper alt style1">
        <div class="inner">
            <h2 class="major">Dodatkowe informacje</h2>
            <p>Sprawdź też:</p>
            <section class="features">
                <article>
                    <a class="image"><img src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/images/plan.jpg" alt="" /></a>
                    <h3 class="major">Plan zajęć</h3>
                    <p></p>
                    <a href="#" class="special">Wkrótce</a>
                </article>
                <article>
                    <a class="image"><img src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/images/kadra.jpg" alt="" /></a>
                    <h3 class="major">Kadra nauczycielska</h3>
                    <p></p>
                    <a href="#" class="special">wkrótce</a>
                </article>

            </section>
        </div>
    </section>

</section>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.scrollex.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/browser.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/breakpoints.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/util.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/main.js"><?php echo '</script'; ?>
>
</body>
</html><?php }
}
