<?php
/* Smarty version 3.1.30, created on 2018-09-27 10:34:31
  from "C:\xampp\htdocs\projekt\app\views\grading.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bac9617419366_78146465',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '73c24fc77dfc7269c37552f4e76535443df8d0a5' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\grading.tpl',
      1 => 1538037190,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:gradesList.tpl' => 1,
  ),
),false)) {
function content_5bac9617419366_78146465 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/css/main.css" />
</head>
<body>
<div class="inner">
    <h2 class="major">Lista ocen <?php echo $_smarty_tpl->tpl_vars['uczen']->value[0]['Imie'];?>
 <?php echo $_smarty_tpl->tpl_vars['uczen']->value[0]['Nazwisko'];?>
</h2>
    <h3><button id="myBtn">Dodaj ocenę</button>
        <!-- The Modal -->
        <div id="myModal" class="modal">
        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <h2>Dodaj nową ocenę</h2>
            </div>
            <div class="modal-body">
                <p><div id="contact3" class="contact3">
                    <form class="form"  method = "post" action = "<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
addGrade">
                        <div class = "fields">
                            <div class = "field">
                                <label>Ocena</label>
                                <input type="text" name="Ocena" placeholder="Ocena" id="Ocena" required/>
                            </div>

                            <div class = "field">
                                <label>Komentarz</label>
                                <input type="text" name="Komentarz" placeholder="Komentarz" id="Komentarz" required/>
                            </div>
                            <div class="field">
                                <button class="formSubmit4" name="submit" type="submit">Zatwierdź</button>
                                <span class="close">&times;</span>
                            </div>
                            <input type="hidden" name="ID_Ucznia" id="ID_Ucznia" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
                        </div>
                        <div class="emailFormAlert"></div>
                    </form>

                </div></p>
            </div>
        </div>
        </div>
        <a class="button" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
studentList">Wróc do listy uczniów</a>
        <button class="triggerMessage" onclick="modalClicked();">Wyślij wiadomość</button>
        <div class="Message">
            <div class="Message-content">
                <span class="Message-button" onclick="modalClicked();"  >×</span>
                <h1>Wiadomość</h1>
                <h1><textarea id="wiadomosc"></textarea></h1>
                <button id="submitMessage" class="button" onclick="sendMessage('<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
addMessage&id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
')">Wyślij</button>
            </div>
        </div>
    </h3>
        <input id="search" type="text" placeholder="Wyszukaj..." onkeyup="ajaxPostForm('<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
reloadGrades', 'GradesContainer');">
    <div class ="GradesContainer" id="GradesContainer">
        <input type="hidden" id="wartosc" value="<?php echo $_smarty_tpl->tpl_vars['ilosc']->value;?>
">
    <?php $_smarty_tpl->_subTemplateRender("file:gradesList.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
</div>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/jquery.scrollex.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/browser.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/breakpoints.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/util.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/lista.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/przycisk.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/message.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/grade.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/updateGrade.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/deleting.js"><?php echo '</script'; ?>
>
</body>
</html>
<?php }
}
