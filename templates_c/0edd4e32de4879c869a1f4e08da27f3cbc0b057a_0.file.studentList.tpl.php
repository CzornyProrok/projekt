<?php
/* Smarty version 3.1.30, created on 2018-09-27 08:36:25
  from "C:\xampp\htdocs\projekt\app\views\studentList.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bac7a692fffa3_55954806',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0edd4e32de4879c869a1f4e08da27f3cbc0b057a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\studentList.tpl',
      1 => 1537964179,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bac7a692fffa3_55954806 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table>
    Znaleziono <?php echo $_smarty_tpl->tpl_vars['iloscWynikow']->value;?>
 wyników
    <thead>
    <tr>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>PESEL</th>
        <th>Klasa</th>
        <th>Oddział</th>
    </tr>
    </thead>
    <tbody id="myTable">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lista']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
        <tr><th><?php echo $_smarty_tpl->tpl_vars['l']->value['Imie'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['l']->value['Nazwisko'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['l']->value['Pesel'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['l']->value['Klasa'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['l']->value['Oddzial'];?>
</th><td><a class="button" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
studentEdit&id=<?php echo $_smarty_tpl->tpl_vars['l']->value['ID_Ucznia'];?>
">Edytuj</a><a class="button" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
studentDelete&id=<?php echo $_smarty_tpl->tpl_vars['l']->value['ID_Ucznia'];?>
" onclick="return deleteFunction()">Usuń</a><a class="button" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
showGrades&id=<?php echo $_smarty_tpl->tpl_vars['l']->value['ID_Ucznia'];?>
">Oceny/Wiadomości</a></td></tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </tbody>
</table>
<ul class="pagination">
    <?php ob_start();
echo $_smarty_tpl->tpl_vars['iloscStron']->value;
$_prefixVariable1=ob_get_clean();
$_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int) ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? $_prefixVariable1+1 - (1) : 1-($_prefixVariable1)+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0) {
for ($_smarty_tpl->tpl_vars['foo']->value = 1, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++) {
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration == 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration == $_smarty_tpl->tpl_vars['foo']->total;?>
        <li><input type="button" id="strona" onclick="ajaxPagination('<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
reloadStudent&currentSite=<?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
', 'studentContainer')" value="<?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
"></li>
    <?php }
}
?>

</ul><?php }
}
